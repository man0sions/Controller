#php controller 抽象类


## Install
```
composer require man0sions/controller

```

## useage


```

class Home extends \LuciferP\Controller\Controller{


    /**
     *  只渲染包含layout 的 html
     * @return string
     */
    public function index(){

        echo  $this->render(['name' => 'zhangsan', 'age' => 20]);
    }

    /**
     * 调用response渲染数据
     *
     * @throws \Exception
     */
    public function index2(){

        $this->response->status(200)->type('text/html')->render(BASE_PATH . "/views/view.php", ['name' => 'zhangsan', 'age' => 20]);

    }

    /**
     * 把包含在layout 的 html一起渲染的数据交给response返回
     */
    public function index3(){

        $ret = $this->render(['name'=>'zhangsan']);
        $this->response->status(403)->type('text/html')->send($ret);
    }
}


$home = new \LuciferP\demo\Home('Home','index',new \LuciferP\Http\Request(),new \LuciferP\Http\Response());
$home->index();
//$home->index3();
//$home->index3();
```